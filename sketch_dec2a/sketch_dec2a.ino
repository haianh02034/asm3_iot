#define BLYNK_TEMPLATE_ID "TMPL6m0Qeimag"
#define BLYNK_TEMPLATE_NAME "Product2"
#define BLYNK_AUTH_TOKEN "2GPd6bYXkexGyE3L8esIFZGKS32Wae7J"

#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

#define Sensor D0
#define LED D1
// #define LED_2 D2
// #define Sensor_2 D4

char auth[] = BLYNK_AUTH_TOKEN;
char ssid[] = "DESKTOP-HHRTFVJ 9870";
char pass[] = "12345678";

bool sensorEnabled = true;

BLYNK_WRITE(V2) {
  sensorEnabled = param.asInt();
}

void setup() {
  Serial.begin(9600);
  pinMode(Sensor, INPUT);
  pinMode(LED, OUTPUT);
  // pinMode(LED_2, OUTPUT);
  // pinMode(Sensor_2, INPUT);
  Blynk.begin(auth,ssid,pass, "blynk.cloud", 80);
}

void loop() {
  Blynk.run();

  if (sensorEnabled) { 
    int Value = analogRead(Sensor);
    Serial.print("Sensor value: ");
    Serial.println(Value);
    Blynk.virtualWrite(V0, Value);

    if (Value > 500) {  
      digitalWrite(LED, HIGH);
      // digitalWrite(LED_2, HIGH);
      Blynk.virtualWrite(V1, LOW);
    } else {
      digitalWrite(LED, LOW);
      // digitalWrite(LED_2, LOW);
      Blynk.virtualWrite(V1, LOW);
      delay(5000);
    }
  }

  delay(1000);
}